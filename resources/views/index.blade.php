<!doctype html>
<html lang="en">
  <head>
      <meta charset="UTF-8">
      <title>home</title>
      <link rel="stylesheet" href="/css/app.css"/>
      <script>
        window.onload(
          var req = new XMLHttpRequest();
          var url = "https://images-api.nasa.gov";
          req.open("GET", url);
          req.send();
          req.addEventListener("load", function(){
            if(req.status == 200 && req.readyState == 4){
    	      var response = JSON.parse(req.responseText);
            document.getElementById("title").style.backgroundImage = response.title;
            document.getElementById("img").src = response.hdurl
            }
          })
        );
      </script>
  </head>
  <body>
    <h1>NASA</h1>
    <div id="filler1">filler</div>
    <div id="filler2">filler</div>
    <input type="text" name="search" id="search" placeholder="search">
    <div id="checkbox">
      <div>
        <input type="checkbox" name="images" value="images" id="images">
        <label for="images">Images</label>
      </div>
      <div>
        <input type="checkbox" name="audio" value="audio" id="audio">
        <label for="audio">Audio</label>
      </div>
    </div>

    <!--
    Attempts made to use the laravel method of showing the data
    $images variable is used to store the data from the nasa api
    For every field in the api, each will have a div , h3 and img tag
    Each H3 tag will use the fields title and each img tag will use the image field as the image source

    <section id="row1">
      @if(isset ($images))
        <div>
          @foreach($images as $image)
            <h3>{{$images->title}}</h3>
            <img src="{{$images->image}}" alt="">
          @endforeach
        </div>
      @endif
    -->
    </section>
    <div id="row1">
      <section>
        <h3 id="title">Section1</h3>
        <img src="" alt="image of volcano" id="img">
      </section>
      <section>
        <h3 id="title">Section2</h3>
      </section>
      <section>
        <h3 id="title">Section3</h3>
      </section>
      <section>
        <h3 id="title">Section4</h3>
      </section>
    </div>
    <div id="row1">
      <section>
        <h3>section1</h3>
      </section>
      <section>
        <h3>section2</h3>
      </section>
      <section>
        <h3>section3</h3>
      </section>
      <section>
        <h3>section4</h3>
      </section>
    </div>
  </body>
</html>
