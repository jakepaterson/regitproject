<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client as GuzzleHttpClient;
use GuzzleHttp\Exception\RequestException;
use App\Http\Requests;

class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     //Attempts made at using Guzzle to consume the external API and use it's data
  //  public funtion guzzle(){
      //  $client = new GuzzleHttpClient();
      //  $apiRequest = $client->request('GET','https://images-api.nasa.gov', [

      //    ]);

      //  }

      //$client = new Client();
      //$result = $client->post('https://images-api.nasa.gov',[
        //'$images = ::all();''
      //]);

    public function index()
    {
        return view('index');
        //return view('index', ['images' => $images]);
    }
}
